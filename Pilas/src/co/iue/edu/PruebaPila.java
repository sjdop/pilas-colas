/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.iue.edu;

/**
 *
 * @author salas
 */
public class PruebaPila {
    
    public static void main(String... args) {
        Pila pila = new Pila();
        
        System.out.println(pila.apilar(10));
        System.out.println(pila.apilar(20));
        System.out.println(pila.apilar(30));
        System.out.println(pila.apilar(40));
        System.out.println(pila.apilar(50));
        System.out.println(pila.apilar(60));
        System.out.println(pila.apilar(70));
        System.out.println(pila.apilar(80));
        System.out.println(pila.apilar(90));
        
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
        System.out.println(pila.desapilar());
    }
    
}
