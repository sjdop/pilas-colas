/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.iue.edu;

/**
 *
 * @author salas
 */
public class Pila {
    
    private Object arr[];
    private int tope;
    private int max;
    
    public Pila() {
        max = 7;
        tope = 0;
        arr = new Object[max];
    }
    
    public Pila(int max) {
        this.max = max;
        tope = 0;
        arr = new Object[max];
    }
    
    private boolean estaLlena() {
        return tope == max;
    }
    
    private boolean estaVacia() {
        return tope == 0;
    }
    
    public String apilar(Object dato) {
        if (estaLlena()) {
            return "Pila llena";
        } else {
            arr[tope] = dato;
            tope++;
            return "Se apilo " + arr[tope-1] + "\tTope= " + tope;
        }
    }

    public String desapilar() {
        if (estaVacia()) {
            return "Pila vacia";
        } else {
            tope--;
            Object aux = arr[tope];
            return "Se desapilo " + aux + "\tTope= " + tope;
        }
    }
    
}
