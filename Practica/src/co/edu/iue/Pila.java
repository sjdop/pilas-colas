/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.iue;

/**
 *
 * @author salas
 */
public class Pila {
    
    private char arr[];
    private int tope;
    private int max;
    
    public Pila() {
        max = 7;
        tope = 0;
        arr = new char[max];
    }
    
    public Pila(int max) {
        this.max = max;
        tope = 0;
        arr = new char[max];
    }
    
    private boolean estaLlena() {
        return tope == max;
    }
    
    public boolean estaVacia() {
        return tope == 0;
    }
    
    public boolean apilar(char dato) {
        if (estaLlena()) {
            return false;
        } else {
            arr[tope] = dato;
            tope++;
            return true;
        }
    }

    public char desapilar() {
        if (estaVacia()) {
            return '\0';
        } else {
            tope--;
            return arr[tope];
        }
    }

    public char obtenerUltimo() {
        if (estaVacia()) {
            return '\0';
        } else {
            return arr[tope - 1];
        }
    }
    
    public int obtenerValor() {
        int valor = 0;
        switch (obtenerUltimo()) {
            case ')':
                valor = 0;
                break;
            case '+':
            case '-':
                valor = 1;
                break;
            case '*':
            case '/':
                valor = 2;
                break;
            case '^':
                valor = 3;
                break;
        }
        return valor;
    }
    
    @Override
    public String toString() {
        String text = "";
        for (char c : arr)
            text += c;
        return text;
    }
    
    /** Operadores  Prioridad Exp   Prioridad Pila
     *  ^           4               3
     *  /*          2               2
     *  +-          1               1
     *  )           5               0
     */
    
}
